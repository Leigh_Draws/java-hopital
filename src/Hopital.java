import java.util.ArrayList;

public class Hopital {
    public static void main(String[] args) {

        // Docteur.e.s

        Doctor who = new Doctor("Who", 925, "0980100235488", "DRWHO0980100", "Mystery");

        // Infirmier.ère.s

        Nurse pinpin = new Nurse("Pinpin", 29, "2950281235785", "NPIN2950281");

        // Patients

        Patient michel = new Patient("Michel Tacos", 37, "1860931115230", "MTACOS860931", new ArrayList<Illness>());
        Patient titouan = new Patient("Titouan Carton", 18, "1053655942007", "TCARTO10536", new ArrayList<Illness>());

        // Maladies

        Illness liar = new Illness("Menteur Fou", new ArrayList<Medication>());
        Illness papillon = new Illness("Syndrome du Papillon Bipolaire", new ArrayList<Medication>());
        Illness gastro = new Illness("Gastro-entérite du Phacochère", new ArrayList<Medication>());

        // Medicaments

        Medication bezoard = new Medication("Bézoard", "320mg");
        Medication polynectar = new Medication("Polynectar", "200ml");
        Medication veritaserum = new Medication("Véritaserum", "350ml");
        Medication humotrifyl = new Medication("Humotrifyl", "3 bouchées");
        Medication papillotrex = new Medication("Papillotrex", "Un tout petit peu ...");



        ////// Méthodes sur les instances

        michel.addIllness(liar);

        liar.addMedication(polynectar);
        liar.addMedication(bezoard);
        liar.addMedication(veritaserum);

        liar.removeMedication(polynectar);

        titouan.addIllness(papillon);
        titouan.addIllness(gastro);

        papillon.addMedication(humotrifyl);
        gastro.addMedication(papillotrex);

        // Print

        who.careForPatient(michel);
        who.recordPatientVisit("Le patient nous a été amené par sa famille dans un état singulier après s'être proclamé être un dragon-frite du Congo, prétendant pouvoir terrasser quiconque s'approcherait de lui. Il est à noter que les dragons-frites du Congo sont célèbres pour leur timidité légendaire et leur réticence à se dévoiler en public. Après étude du cas, il ne fait aucun doute que notre patient est atteint de la maladie du menteur fou.");
        System.out.println(michel.getInfo());

        pinpin.careForPatient(titouan);
        System.out.println(titouan.getInfo());
    }
}