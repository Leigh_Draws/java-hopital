public interface Care {

    public void careForPatient(Patient patient);

    public default void recordPatientVisit(String note) {
        System.out.println( "Notes : " + note);
    }
}
