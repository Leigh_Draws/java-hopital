public class Doctor extends MedicalStaff {

    private String specialty;

    public Doctor(String name, int age, String socialSecurityNumber, String employeeId, String specialty) {
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }

    @Override
    public String getRole() {
        return "Docteur";
    }

    // ???????
    @Override
    public void careForPatient(Patient patient) {
        super.careForPatient(patient);
        System.out.println(this.getRole() + " " + this.getName() + " soigne " + patient.getName() + ".");
    }

    @Override
    public void recordPatientVisit(String note) {
        super.recordPatientVisit(note);
    }
}
