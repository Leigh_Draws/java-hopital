abstract public class MedicalStaff extends Person implements Care {

    String employeeId;
    public MedicalStaff(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    // Méthodes de l'interface Care
    @Override
    public void careForPatient(Patient patient) {

    }
    @Override
    public void recordPatientVisit(String note) {
        Care.super.recordPatientVisit(note);
    }

    // Méthode abstraite (pas de corps) elle sera définie dans les classes enfants
    abstract public String getRole();
}
