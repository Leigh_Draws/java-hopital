import java.util.ArrayList;

public class Patient extends Person {

    private String patientId;
    private ArrayList<Illness> illnessList;
    public Patient(String name, int age, String socialSecurityNumber, String patientId, ArrayList<Illness> illnessList) {
        super(name, age, socialSecurityNumber);

        this.patientId = patientId;
        this.illnessList = illnessList;
    }

    public void addIllness( Illness illness ) {
        this.illnessList.add(illness);
    }

    public String getInfo() {
        String illnessInfo = "";
        for (Illness illness : this.illnessList) {
            illnessInfo += illness.getInfo();
        }
        return this.getName() + " " + this.getAge() + " ans. Numéro de Sécurité Sociale :  " + this.getSocialSecurityNumber() + ". \n" + illnessInfo;
    }
}
