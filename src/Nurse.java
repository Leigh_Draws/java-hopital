public class Nurse extends MedicalStaff {
    public Nurse(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
    }

    @Override
    public String getRole() {
        return "Infirmier";
    }

    public void careForPatient(Patient patient) {
        super.careForPatient(patient);
        System.out.println(this.getRole() + " " + this.getName() + " soigne " + patient.getName() + ".");
    }
}
