import java.util.ArrayList;

public class Illness {

    private String name;
    private ArrayList<Medication> medicationList;

    public Illness (String name, ArrayList<Medication> medicationList) {
        this.name = name;
        this.medicationList = medicationList;
    }

    public void addMedication( Medication medication) {
        this.medicationList.add(medication);
    }

    public void removeMedication(Medication medication) {
        this.medicationList.remove(medication);
    }

    public String getInfo() {
        String medInfo = "";
        for (Medication medication : this.medicationList) {
            medInfo += medication.getInfo() + ". ";
        }
        return "Nom de la maladie : " + this.name + ". Traitement : " + medInfo;
    }
}
